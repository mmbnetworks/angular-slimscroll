var gulp = require('gulp');
var closureCompiler = require('gulp-closure-compiler');

gulp.task('default', function() {
    return gulp.src('angular-slimscroll.js')
        .pipe(closureCompiler('angular-slimscroll.min.js'))
        .pipe(gulp.dest('.'));
});
